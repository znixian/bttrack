require 'ipaddr'

class InfoHash
  
  extend Forwardable
  
  def initialize id
    @id = id
  end

  # returns announce details with peer list
  def announce compact, no_peer_id, numwant, exclude=[]
    peer_list = peers(numwant).keep_if do |peer_id, peer|
      !exclude.include? peer_id
    end.map do |peer_id, peer|
      if compact
        [IPAddr.new(peer[:ip]).to_i, peer[:port].to_i].pack('Nn')
      else
        {'ip' => peer[:ip], 'port' => peer[:port].to_i}.tap do |data|
          data['peer id'] = peer_id unless no_peer_id
        end
      end
    end
    { 'interval' => CONF[:announce_interval],
      'min interval' => CONF[:min_interval],
      'peers' => compact ? peer_list.join : peer_list }
  end

  # updates peer's details
  def event! params
    if params["event"] == 'stopped'
      store.delete_peer params['peer_id']
    else
      store.set_peer params['peer_id'], ip: params["ip"],
        downloaded: params["downloaded"].to_i, uploaded: params["uploaded"].to_i,
        left: params["left"].to_i, port: params["port"].to_i
    end
  end

  # https://wiki.theory.org/BitTorrentSpecification#Tracker_.27scrape.27_Convention
  def scrape
    defaults = {downloaded: 0, complete: 0, incomplete: 0}
    {:files => { @id => defaults.merge(store.stats) }}
  end

  def self.all
    TorrentCache.all.map { |t| InfoHash.new t.info_hash }
  end

  # scrape for all torrents
  def self.scrape
    defaults = {downloaded: 0, complete: 0, incomplete: 0}
    {:files => InfoHash.all.each_with_object({}) do |t|
      defaults.merge t.stats
    end}
  end
  
  def pretty_hash
    @id.unpack('H*')[0]
  end

  def_delegators :store, :stats, :peer, :peers

  private

  def store
    @store ||= TorrentCache.load(@id)
  end

end

