
class TorrentCache
	@@torrents = {}
	
	private
	def initialize info_hash
		@info_hash = info_hash
		@peers = {}
		@stats = {}
	end
	
	public
	
	# Load a torrent, given it's info_hash
	def self.load id
		key = id.bytes.join '.' # Otherwise the strings somehow don't work properly as keys
		@@torrents[key] ||= TorrentCache.new id
	end
	
	# Get a list of all the torrents
	def self.all
		@@torrents.values
	end
	
	# Set or update peer's data
	def set_peer peer_id, data
		@peers[peer_id] = data.merge(expires_at: Time.now + CONF[:announce_interval] * 2)
		update_stats!
	end

	# delete given peer
	def delete_peer peer_id
		@peers.delete peer_id
		update_stats!
	end

	# get one peer for a torrent (read only)
	def peer peer_id
		@peers[peer_id]
	end

	# get all peers for a torrent (read only)
	def peers limit = nil
		return @peers.to_a[0...limit].to_h if limit
		@peers
	end
	
	attr_reader :stats
	attr_accessor :info_hash
	
	private
	
	def update_stats!
		@peers.delete_if { |id, peer| peer[:expires_at] < Time.now }
		
		@stats[:complete] = @peers.count { |_, p| p[:left].to_i == 0 }
		@stats[:incomplete] = @peers.count { |_, p| p[:left].to_i != 0 }
		
		# TODO remove when no peers are present
	end
end

